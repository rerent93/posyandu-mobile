import { Navigation } from "react-native-navigation";
import LoginScreen from "../screens/auth/LoginScreen";
import RegisterScreen from "../screens/auth/RegisterScreen";
import RegisterScreen2 from "../screens/auth/RegisterScreen2";
import ListScreen from "../screens/auth/ListScreen";
import MainScreen from "../screens/main/MainScreen";
import App from "../../App";

export function routes(){
    Navigation.registerComponent('App', ()=>App)
    Navigation.registerComponent('LoginScreen', ()=>LoginScreen)
    Navigation.registerComponent('RegisterScreen', ()=>RegisterScreen)
    Navigation.registerComponent('RegisterScreen2', ()=>RegisterScreen2)
    Navigation.registerComponent('ListScreen', ()=>ListScreen)
    Navigation.registerComponent('MainScreen', ()=>MainScreen)

}