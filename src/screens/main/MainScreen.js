
import React, { Component } from 'react';
import {
  DatePickerAndroid,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  View,
  Image,
  TouchableOpacity,
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import colors from '../../utilities/colors';
import helpers from '../../utilities/helpers';
import EncryptedStorage from 'react-native-encrypted-storage';

class MainScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      motherName: '',
      placeOfBirth: '',
      dateOfBirth: '',
      Address: '',
      phoneNumber: '',
      password: ''
    };
  }
 

_onLogout(){
EncryptedStorage.removeItem('accessToken')
Navigation.setRoot({
    root: {
      stack: {
        children: [
          {
            component: {
              name: 'LoginScreen'
            }
          }
        ]
      }
    }
 });
}

render(){
  return (
    <ScrollView keyboardShouldPersistTaps={'handled'}>
      <Text style={styles.header}>
       MAIN SCREEN
      </Text>
      

      <TouchableOpacity
        onPress={() => this._onLogout()}>
        <Text style={styles.registerButton}>keluar</Text>
      </TouchableOpacity>

    </ScrollView>
  );
};
}



const styles = StyleSheet.create({
  header: {
    margin: 16,
    fontSize: 20,
    fontWeight: 'bold',
    color: colors.blackColor
  },
  subHeader: {
    margin: 16,
    marginTop: 0,
    fontSize: 16,
    color: colors.blackColor
  },
  input: {
    height: 40,
    marginHorizontal: 16,
    marginVertical: 8,
    borderBottomWidth: 1,
    padding: 10,
  },
  button: {
    alignItems: 'center',
    backgroundColor: colors.primaryColor,
    padding: 12,
    margin: 16,
    marginVertical: 20,
    borderRadius: 8
  },
  textButton: {
    color: colors.whiteColor,
    fontWeight: 'bold'
  },
  dontHaveAccount: {
    textAlign: 'center',
  },
  registerButton: {
    color: colors.primaryColor,
    textAlign: 'right',
    margin: 16,
  }

});

export default MainScreen;
