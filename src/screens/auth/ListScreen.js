
import React, { Component } from 'react';
import {
    ScrollView,
    StyleSheet,
    Text,
    TextInput,
    View,
    Image,
    TouchableOpacity,
    TouchableOpacityBase,
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import colors from '../../utilities/colors';

class RegisterScreen2 extends Component {

    constructor(props) {
        super(props);
        this.state = {
            datas: []
        };
    }

    componentDidMount() {
        if (this.props.type === 'PROVINSI') {
            this._getDatas('provinces')
        }
        if (this.props.type === 'KABUPATEN KOTA') {
            this._getDatas('cities', `province_id=${this.props.province_id}`)
        }
        if (this.props.type === 'KECAMATAN') {
            this._getDatas('districts', `city_id=${this.props.city_id}`)
        }
        if (this.props.type === 'DESA') {
            this._getDatas('villages', `district_id=${this.props.district_id}`)
        }
        if (this.props.type === 'Posyandu') {
            this._getDatas('posyandus', `village_id=${this.props.village_id}`)
        }
    }

    async _getDatas(type, params = '') {
        try {
            const url = 'https://eposyandu.temanusaha.com/api/service/'
            console.log(`${url}${type}?${params}`);
            const response = await fetch(`${url}${type}?${params}`, {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json'
                }
            })
            const json = await response.json();
            this.setState({ datas: json })
        } catch (error) {
            console.log(error);
        }
    }

    _onNextRegister() {
        Navigation.push(this.props.componentId, {
            component: {
                name: 'RegisterScreen2',
            },
        });
    }

    async _getProvinces() {

        Navigation.push(this.props.componentId, {
            component: {
                name: 'RegisterScreen2',
            },
        });

    }

    _setDatas(item) {
        this.props._setDatas(item, this.props.type)
        Navigation.pop(this.props.componentId)
    }
    render() {
        return (
            <ScrollView keyboardShouldPersistTaps={'handled'}>
                <Text style={styles.header}>
                    LIST {this.props.type}
                </Text>

                {this.state.datas.length > 0 && this.state.datas.map((item, index) => {
                    return (
                        <TouchableOpacity
                            onPress={() => this._setDatas(item)}
                            style={styles.listItem}>
                            <Text>{item.name}</Text>
                        </TouchableOpacity>
                    )
                })}

            </ScrollView>
        );
    };
}


const styles = StyleSheet.create({
    header: {
        margin: 16,
        fontSize: 20,
        fontWeight: 'bold',
        color: colors.blackColor
    },
    listItem: {
        margin: 16,
        padding: 16,
        borderWidth: 1,
        borderColor: colors.blackColor,
        borderRadius: 4,
        marginBottom: 4
    }
});

export default RegisterScreen2;
