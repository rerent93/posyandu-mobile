
import React, { Component } from 'react';
import {
  DatePickerAndroid,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  View,
  Image,
  TouchableOpacity,
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import colors from '../../utilities/colors';
import helpers from '../../utilities/helpers';

class RegisterScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      motherName: '',
      placeOfBirth: '',
      dateOfBirth: '',
      Address: '',
      phoneNumber: '',
      password: ''
    };
  }
  _onLogin() {
    console.log('login');
  }
  _onNextRegister() {
    const form1 = this.state
    Navigation.push(this.props.componentId, {
      component: {
        name: 'RegisterScreen2',
        passProps: {
          form1
        }
      },
    });
  }
  async _openDateTimePicker() {
    try {
      const {
        action,
        year,
        month,
        day,
      } = await DatePickerAndroid.open({
        mode: 'spinner',
        date: new Date()
      });
      if (action !== DatePickerAndroid.dismissedAction) {
        this.setState({ dateOfBirth: helpers.translateDate(year, month, day) })
      }
    }
    catch ({ code, message }) {
      console.warn('Cannot open data picker', message);
    }
  }



render(){
  return (
    <ScrollView keyboardShouldPersistTaps={'handled'}>
      <Text style={styles.header}>
        DAFTAR
      </Text>

      <Text style={styles.subHeader}>
        Silahkan isi semua form dibawah ini
      </Text>

      <TextInput
        style={styles.input}
        value={this.state.motherName}
        onChangeText={(motherName) => this.setState({ motherName })}
        placeholder="Nama Lengkap Ibu"
        keyboardType="default"
        autoCapitalize='words'
      />
      <View style={{ flexDirection: 'row', flex: 1 }}>
        <TextInput
          style={[styles.input, { flex: 1 }]}
          value={this.state.placeOfBirth}
          onChangeText={(placeOfBirth) => this.setState({ placeOfBirth })}
          placeholder="Tempat Lahir"
          keyboardType="default"
          autoCapitalize='words'
        />
        <TextInput
          style={[styles.input, { flex: 1 }]}
          value={this.state.dateOfBirth}
          onChangeText={(dateOfBirth) => this.setState({ dateOfBirth })}
          placeholder="Tanggal Lahir"
          keyboardType="default"
          autoCapitalize='words'
          onFocus={() => this._openDateTimePicker()}
        />
      </View>

      <TextInput
        style={styles.input}
        value={this.state.Address}
        onChangeText={(Address) => this.setState({ Address })}
        placeholder="Alamat"
        keyboardType="default"
        autoCapitalize='words'
      />
      <TextInput
        style={styles.input}
        value={this.state.phoneNumber}
        onChangeText={(phoneNumber) => this.setState({ phoneNumber })}
        placeholder="Nomor Telepon"
        keyboardType="numeric"
      />
      <TextInput
        style={styles.input}
        value={this.state.password}
        onChangeText={(password) => this.setState({ password })}
        placeholder="Masukan Password"
        keyboardType="default"
        autoCapitalize='none'
        secureTextEntry
      />

      <TouchableOpacity
        onPress={() => this._onNextRegister()}>
        <Text style={styles.registerButton}>Selanjutnya</Text>
      </TouchableOpacity>

    </ScrollView>
  );
};
}



const styles = StyleSheet.create({
  header: {
    margin: 16,
    fontSize: 20,
    fontWeight: 'bold',
    color: colors.blackColor
  },
  subHeader: {
    margin: 16,
    marginTop: 0,
    fontSize: 16,
    color: colors.blackColor
  },
  input: {
    height: 40,
    marginHorizontal: 16,
    marginVertical: 8,
    borderBottomWidth: 1,
    padding: 10,
  },
  button: {
    alignItems: 'center',
    backgroundColor: colors.primaryColor,
    padding: 12,
    margin: 16,
    marginVertical: 20,
    borderRadius: 8
  },
  textButton: {
    color: colors.whiteColor,
    fontWeight: 'bold'
  },
  dontHaveAccount: {
    textAlign: 'center',
  },
  registerButton: {
    color: colors.primaryColor,
    textAlign: 'right',
    margin: 16,
  }

});

export default RegisterScreen;
