/**
 * @format
 */

 import { Navigation } from "react-native-navigation";
//import {AppRegistry} from 'react-native';
import App from './App';
import { routes } from "./src/navigation/routes";
import colors from "./src/utilities/colors";
//import {name as appName} from './app.json';

//AppRegistry.registerComponent(appName, () => App);
Navigation.registerComponent('com.myApp.WelcomeScreen', () => App);
Navigation.setDefaultOptions({
  statusBar: {
    visible: true,
    style: 'light',
    backgroundColor: colors.blackColor,
  },
  topBar: {
    title: {
      text: '',
      color: colors.whiteColor,
    },
    background: {
      color: colors.primaryColor,
    },
    backButton: {
      color: colors.whiteColor,
    },
    animate:false,
  },
  animations: {
    push: {
        waitForRender: true,
        topBar: {
            alpha: {
                from: 0,
                to: 1,
            },
        },
        bottomTabs: {
            alpha: {
                from: 0,
                to: 1,
            },
        },
        content: {
            alpha: {
                from: 0,
                to: 1,
            },
        },
    },
    pop: {
        waitForRender: true,
        topBar: {
            alpha: {
                from: 1,
                to: 0,
            },
        },
        bottomTabs: {
            alpha: {
                from: 1,
                to: 0,
            },
        },
        content: {
            alpha: {
                from: 1,
                to: 0,
            },
        },
    },
    setRoot: {
        waitForRender: true,
        alpha: {
            from: 0,
            to: 1,
            duration: 300,
        },
    }
},
})

routes()
Navigation.events().registerAppLaunchedListener(() => {
   Navigation.setRoot({
     root: {
       stack: {
         children: [
           {
             component: {
               name: 'App'
             }
           }
         ]
       }
     }
  });
});
