
import React, { Component } from 'react';
import {
  ActivityIndicator,
  StyleSheet, View,

} from 'react-native';
import LoginScreen from './src/screens/auth/LoginScreen';
import RegisterScreen from './src/screens/auth/RegisterScreen';
import colors from './src/utilities/colors';
import EncryptedStorage from 'react-native-encrypted-storage';
import { Navigation } from 'react-native-navigation';
class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
    };
  }

  componentDidMount(){
    this._checkToken()
  }

  async _checkToken(){
    try {
      const accessToken = await EncryptedStorage.getItem("accessToken");
      if (accessToken) {
        this._setRoot('MainScreen')
      } else {
        this._setRoot('LoginScreen')
      }
    } catch (error) {
      console.log(error);
    }
  }

  _setRoot(screenName) {
    Navigation.setRoot({
      root: {
        stack: {
          children: [
            {
              component: {
                name: screenName
              }
            }
          ]
        }
      }
   });
  }


render(){
  return (
   <View style={styles.container}>
   <ActivityIndicator
    size={'large'}
    color={colors.primaryColor}
    />
   </View>
  );
};
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
});

export default App;
